package controllers

import (
	"github.com/revel/revel"
	"fmt"
	"vlc-http-go/httpvlc"
	"runtime"
	"net/url"
	"encoding/json"
	"log"
	"os"
	"bufio"
	"strings"
	"net/http"
)


type App struct {
	*revel.Controller
}

type SocketData struct {
	Second string
}
const host, port, password = "localhost", "8080", "test4"

//type WebSocket struct {
//	*revel.Controller
//}

var client = &http.Client{}

func getBasicAuthReguest(passwd string, url string) *http.Request {
	req, _ := http.NewRequest("GET", url, nil)
	req.SetBasicAuth("", passwd)
	return req
}

func getEscapedDirUri(dirPath string) string {
	var dirUri = "file:///" + dirPath
	return url.QueryEscape(dirUri)
}

func getDefaultDirUriVideo(file string) string {
	var a=  getDefaultDirUri() + "\\" + file;
	return a;
}
func getDefaultDirUri() string {

	var dirPath string

	switch runtime.GOOS {
	case "windows":
		dirPath = "C:\\DEV\\samples"

	case "darwin":
		dirPath = "/media/pavel/Windows2/DEV/samples"

	case "linux":
		dirPath = "/media/pavel/Windows2/DEV/samples"

	default:
		dirPath = "/media/pavel/Windows2/DEV/samples"
	}

	//file%3A%2F%2F%2FC%3A%5CDEV%5Csamples
	return dirPath;
}

func encode( uri string ) string{
	return getEscapedDirUri(uri)
}

func (c App) Index() revel.Result {
	//vlc --extraintf http --http-host localhost --http-port 8080 --http-password test4
	var s []interface{}
	//const dirPath = "C:\\DEV\\samples" // TODO: why I cannot include it to function?

	var browseResult, err = httpvlc.GetDirContent(getEscapedDirUri(getDefaultDirUri()), host, port, password)

	data := make(map[string]interface{})
	data["error"] = err
	data["browseResult"] = browseResult
	for _, v := range browseResult.Element {
		if v.Type == "file" {
			var a = strings.Split(v.Name, ".")
			var file = map[string]string{"name": v.Name, "type": a[1]}
			s = append(s, file)
		}
	}
	return c.RenderJSON(s)
}


func (c App) Play() revel.Result {
	var ab = c.Params.Query.Get("video");
	var a = getEscapedDirUri(getDefaultDirUriVideo(ab));
	fmt.Println(a)
	var newStatus, err2 = httpvlc.PlayUri(a, host, port, password)
	if (err2 != nil) {
		fmt.Println("Error in start playing")
		println(err2)
	}
	println("Start Play Result:")
	printMarshalledStatus(newStatus)

	return c.RenderJSON(a)
}

func (c App) Parse() revel.Result {
	var ab = c.Params.Query.Get("subtitle");
	var bufer = scaner(getDefaultDirUriVideo(ab));

	return c.RenderJSON(bufer)
}

func (c App) Status() revel.Result {
	var a, _ = GetVlcStatus("/dirka", host, "8080", password)

	fmt.Println("Hi!")
	return c.RenderJSON(a)
}

	func (c App) AddSubtitle() revel.Result {
	var subtitlefile = c.Params.Query.Get("subtitlefile");

	var a, e = AddSubtitle("/dirka", host, "8080", password, subtitlefile)

	fmt.Println(a)
	return c.RenderJSON(e)
}

func printMarshalledStatus(result *httpvlc.VlcStatus) {
	data, err := json.MarshalIndent(result, "", "    ")
	if (err != nil) {
		log.Fatalf("JSON Marshalling failed: %s", err)
	}
	fmt.Printf("%s\n", data)
}

func scaner(path string) ([]interface{}){
	var bufer []interface{}
	f, err := os.Open(path)
	if err != nil {
		return bufer
	}
	defer f.Close()

	// Splits on newlines by default.
	scanner := bufio.NewScanner(f)

	line := 1
	flag := false
	// https://golang.org/pkg/bufio/#Scanner.Scan
	for scanner.Scan() {
		if(flag){
			var a = scanner.Text()
			var b = strings.Split(a, ",")
			var c = make(map[string]interface{})
			c["Layer"] = b[0]
			c["Start"] = b[1]
			c["End"] = b[2]
			c["Style"] = b[3]
			c["Name"] = b[4]
			c["MarginL"] = b[5]
			c["MarginR"] = b[6]
			c["MarginV"] = b[7]
			c["Effect"] = b[8]
			c["Text"] = b[9]
			bufer = append(bufer, c)
		}

		if strings.Contains(scanner.Text(), "Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text") {
			flag = true
		}

		line++
	}

	if err := scanner.Err(); err != nil {
		// Handle the error
	}
	return bufer
}


func GetVlcStatus(dirUri string, host string, port string, password string) (interface{}, error) {

	var fetchUrl = "http://"+host+":"+port+"/requests/status.json"
	fmt.Println(fetchUrl);
	//var fetchUrl = "http://:"+password+"@"+host+":"+port+"/requests/browse.json?uri=" + dirUri
	//var fetchUrl = "www.google.com"
	log.Println("getting dir content. url = " + fetchUrl)
	resp, ee := client.Do(getBasicAuthReguest(password, fetchUrl))
	//resp, err := http.Get(fetchUrl)
	if (resp == nil) {
		return nil, ee
	}

	// We must close resp.Body on all execution paths
	if resp.StatusCode != http.StatusOK {
		resp.Body.Close()
		return nil, fmt.Errorf("browse query failed: %s", resp.Status)
	}

	var result httpvlc.VlcStatus
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		//resp.Body.Close()
		//return nil, err
	}

	resp.Body.Close()
	return &result, nil
}

func AddSubtitle(dirUri string, host string, port string, password string, file string) (interface{}, error) {
	var fetchUrl = "http://"+host+":"+port+"/requests/status.json?command=addsubtitle&val=" + getDefaultDirUriVideo(file)
	fmt.Println(fetchUrl);
	//var fetchUrl = "http://:"+password+"@"+host+":"+port+"/requests/browse.json?uri=" + dirUri
	//var fetchUrl = "www.google.com"
	log.Println("getting dir content. url = " + fetchUrl)
	_, er := client.Do(getBasicAuthReguest(password, fetchUrl))
	fmt.Println(er);



	var fetchUrl2 = "http://"+host+":"+port+"/requests/status.json?command=subtitle_track&val=2"
	_, er2 := client.Do(getBasicAuthReguest(password, fetchUrl2))


	return fetchUrl, er2
}
//
//func (c App) Socket(ws revel.ServerWebSocket) revel.Result {
//	server, err := socketio.NewServer(nil)
//	if err != nil {
//		log.Fatal(err)
//	}
//	server.On("connection", func(so socketio.Socket) {
//		log.Println("on connection")
//		so.Join("vlc")
//		so.On("status", func(msg string) {
//			//m := make(map[string]interface{})
//			//m["a"] = "test"
//			//e := so.Emit("cn1111", m)
//			//
//			//fmt.Println("\n\n")
//			//
//			var a = SocketData{Second: "sdf"};
//			log.Println("emit:", so.Emit("status",  a))
//			so.BroadcastTo("vlc", "status", msg)
//		})
//		// Socket.io acknowledgement example
//		// The return type may vary depending on whether you will return
//		// For this example it is "string" type
//		//so.On("status with ack", func(msg string) string {
//		//	return msg
//		//})
//		so.On("disconnection", func() {
//			log.Println("on disconnect")
//		})
//	})
//	server.On("error", func(so socketio.Socket, err error) {
//		log.Println("error:", err)
//	})
//
//	http.Handle("/socket.io/", server)
//	http.Handle("/", http.FileServer(http.Dir("./asset")))
//	log.Println("Serving at localhost:5000...")
//	log.Fatal(http.ListenAndServe(":5000", nil))
//
//	return nil
//}
//
////func (c WebSocket) Room(user string) revel.Result {
////	return c.Render(user)
////}
//
//func (c App) SocketIo(data string) revel.Result {
//	return c.RenderJSON(data)
//}